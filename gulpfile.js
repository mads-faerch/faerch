var gulp = require('gulp');
var postcss = require('gulp-postcss');
var autoprefixer = require('autoprefixer');
var csswring = require('csswring');
var sass = require('gulp-sass');
var rename = require('gulp-rename');
var pug = require('gulp-pug');
var browserSync = require('browser-sync').create({port: 3005});
var imagemin = require('gulp-imagemin');

// Start server and watch pug, html and sass files
gulp.task('serve', ['styles', 'templates', 'scripts', 'fonts'], function() {
  gulp.watch('**/*.sass', ['styles']);
  gulp.watch('**/*.pug', ['templates']);
  gulp.watch('assets/images/**/*', ['images']);
  gulp.watch('assets/fonts/**/*', ['fonts']);
  gulp.watch('assets/scripts/**/*', ['scripts']);
  gulp.watch('build/*.html').on('change', browserSync.reload);
});

// Compile, prefix and minify sass. Then move to build/css
gulp.task('styles', function() {
  var processors = [
    csswring,
    autoprefixer({browsers:['last 3 version']})
  ];

  return gulp.src('assets/sass/main.sass')
    .pipe(sass())
    .pipe(postcss(processors))
    .pipe(rename('css/styles.css'))
    .pipe(gulp.dest('./build'))
    .pipe(browserSync.stream());
});

// Compile pug files and move to build
gulp.task('templates', function() {
  gulp.src('templates/**/!(_)*.pug', {base: 'templates'})
    .pipe(pug({
      pretty: true
    }))
    .pipe(gulp.dest('./build'));
});

//Optimize images
gulp.task('images', function() {
  gulp.src('assets/images/**/*', {base: 'assets/images'})
    .pipe(imagemin({
      progressive: true,
      svgoPlugins: [{removeViewbox: false}]
    }))
    .pipe(gulp.dest('./build/images'));
});

// Move scripts file (add concatenation at a later point)
gulp.task('scripts', function() {
  gulp.src('assets/scripts/**/*', {base: 'assets/scripts'})
    .pipe(gulp.dest('./build/scripts'));
});

// Move font files
gulp.task('fonts', function() {
  gulp.src('assets/fonts/**/*', {base: 'assets/fonts'})
    .pipe(gulp.dest('./build/fonts'));
});

gulp.task('default', ['serve']);